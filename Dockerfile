FROM openjdk:7

WORKDIR /ds-main

COPY ./target .
COPY ./ run.sh
RUN ["chmod", "+777", "./run.sh"]

EXPOSE 8080

CMD ["./run.sh"]
